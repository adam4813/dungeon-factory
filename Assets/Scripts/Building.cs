﻿using UnityEngine;
using UnityEngine.Serialization;


[CreateAssetMenu(fileName = "Building", menuName = "Building/Building", order = 1)]
public class Building : ScriptableObject
{
    public int width;
    public int height;
    public string buildingName;
}