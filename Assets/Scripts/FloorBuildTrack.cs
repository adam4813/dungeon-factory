using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class FloorBuildTrack : MonoBehaviour
{
    private Dictionary<int, Building> _builtFloorSections = new();
    public Building selectedBuilding;

    private InputAction _click;

    private void Awake()
    {
        _click = new InputAction(binding: "<Mouse>/leftButton");
        _click.performed += ctx =>
        {
            Debug.Log("OnClick");
            var mainCamera = Camera.main;
            if (!mainCamera) return;

            Vector3 mousePos = Mouse.current.position.ReadValue();
            if (!Physics.Raycast(mainCamera.ScreenPointToRay(mousePos), out var hit)) return;

            if (hit.collider.TryGetComponent<FloorBuildTrack>(out var clickedFloorBuildTrack))
            {
                mousePos.z = mainCamera.nearClipPlane;
                Vector3 worldPos = mainCamera.ScreenToWorldPoint(mousePos);
                Debug.Log($"OnClick floor track @ {worldPos}");
                clickedFloorBuildTrack.OnAddBuilding(0, selectedBuilding);
            }
        };
        _click.Enable();
    }

    private void OnDestroy()
    {
        _click.Disable();
    }

    public void OnAddFloorSection(int x)
    {
        if (_builtFloorSections.ContainsKey(x))
        {
            return;
        }

        _builtFloorSections.Add(x, null);
    }

    public void OnAddBuilding(int x, Building building)
    {
        for (var i = x; i < x + building.width; i++)
        {
            if (!_builtFloorSections.ContainsKey(i) || _builtFloorSections[i] != null)
            {
                return;
            }
        }

        for (var i = x; i < x + building.width; i++)
        {
            _builtFloorSections[i] = building;
        }
    }
}